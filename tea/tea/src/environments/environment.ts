// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
      apiKey: "AIzaSyCLPocUqMwHcRqELQYKE3E5YFhdaaM5ToY",
      authDomain: "data-5dea6.firebaseapp.com",
      databaseURL: "https://data-5dea6.firebaseio.com",
      projectId: "data-5dea6",
      storageBucket: "data-5dea6.appspot.com",
      messagingSenderId: "115747497464",
      appId: "1:115747497464:web:261e3566ad9da49d6ba20d"
    }  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
