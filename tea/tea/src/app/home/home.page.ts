import { Component, OnInit } from '@angular/core';
import { Teams } from '../';
import { DataService } from '../services/data.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  data: Teams[];
  constructor(private dataService: DataService) {}
  ngOnInit() {
    this.dataService.getAllTeams().subscribe(res => {
    
      this.data = res;
    });
  }


}
