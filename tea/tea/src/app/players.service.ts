import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Players} from '../app/models/players';


@Injectable({
  providedIn: 'root'
})
export class PlayersService {


  db = null;
  
  constructor(db: AngularFirestore){
    this.db = db;
  }


  
  getByTeam(team_id: string){
    var x = this. db.collection('players', ref => ref.where('team_id', '==', team_id));
    var players = x.snapshotChanges().pipe(map(
       actions => {
        return actions.map( a => {
          const id = a.payload.doc.id;
          const data = a.payload.doc.data();
          
          return {id, ...data}

        });
      }
    ))
    return players;
  }
  
}


