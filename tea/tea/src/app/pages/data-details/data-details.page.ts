import { Component, OnInit } from '@angular/core';
import { Teams } from '../../models/teams';
import { Players } from '../../models/players';
import { DataService } from '../../services/data.service';
import { PlayersService } from '../../players.service';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-data-details',
  templateUrl: './data-details.page.html',
  styleUrls: ['./data-details.page.scss'],
})
export class DataDetailsPage implements OnInit {
  public length: any;


   data : Players = {

    name : '',
    pic: '',
    surname: '',
    team_id: ''


    
  };
  dataId = null;
  player: Players[];

  constructor(private route: ActivatedRoute, private nav: NavController,
    private playersService: PlayersService, private loadingController: LoadingController) {

  }
  ngOnInit() {
    this.dataId = this.route.snapshot.params['id'];
    if (this.dataId) {
      this.loadData();
      console.log(this.dataId);
    }
  }
  async loadData() {
    const loading = await this.loadingController.create({
      message: "Cargando..."
    });
    await loading.present();
    this.playersService.getByTeam(this.dataId).subscribe(res => {
      loading.dismiss();
      this.player = res;
      console.log( 'players ', this.player);
    })
  }


 




  

}
