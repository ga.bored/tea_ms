import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Teams} from '../models/teams';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private dataCollection : AngularFirestoreCollection<Teams>;
  private teams : Observable<Teams[]>;

  constructor(db: AngularFirestore) {
    this.dataCollection = db.collection<Teams>('teams');
    this.teams = this.dataCollection.snapshotChanges().pipe(map(
      actions => {
        return actions.map( a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return {id, ...data}
        });
      }
    ))
  }
  getAllTeams() {
    return this.teams;
  }
  getTeams(id:string) {
    return this.dataCollection.doc<Teams>(id).valueChanges();
  }  
}
